package Model;

/**
 * Objekt das jede Zeile erhalten muss es wird in PingRow gespeichert.
 *
 * Created by pludewig on 03.05.2016.
 */
public class EinstellungenPings {

    boolean unendlichPingen;
    int AnzahlPings;
    int Port;
    public EinstellungenPings() {
        AnzahlPings= 100;
        Port= 80;
        unendlichPingen = false;
    }

    public EinstellungenPings(boolean unendlichPingen, int anzahlPings,int port) {
        this.unendlichPingen = unendlichPingen;
        this.AnzahlPings = anzahlPings;
        this.Port=port;
    }

    public boolean isUnendlichPingen() {
        return unendlichPingen;
    }

    public void setUnendlichPingen(boolean unendlichPingen) {
        this.unendlichPingen = unendlichPingen;
    }

    public int getAnzahlPings() {
        return AnzahlPings;
    }

    public void setAnzahlPings(int anzahlPings) {
        AnzahlPings = anzahlPings;
    }

    public int getPort() {
        return Port;
    }

    public void setPort(int port) {
        Port = port;
    }
}
