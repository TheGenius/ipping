package Model;

import javax.swing.*;
import java.util.ArrayList;

public class PingRow {



    String IPAddress;
	long Pingsinsgesamt;
	long gefundenePakete;
	long verlorenePakete;
    JButton  Button;
	ArrayList<String> timesstamps = new ArrayList<>();
    int rownumber;
    EinstellungenPings einstellungenPings;

    //Thread Handling
    boolean runstatus;

    public PingRow(String ipaddress,JButton tmpbutton, long gefundenePakete, long verlorenePakete, int rownumber, EinstellungenPings einstellungenPings){
        this.einstellungenPings = einstellungenPings;
        this.setIPAddress(ipaddress);
        this.setButton(tmpbutton);
        this.setGefundenePakete(gefundenePakete);
        this.setVerlorenePakete(verlorenePakete);
        this.setRownumber(rownumber);
    }

    public PingRow() {

    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public int getRownumber() {
        return rownumber;
    }

    public void setRownumber(int rownumber) {
        this.rownumber = rownumber;
    }

    public long getPingsinsgesamt() {
		return Pingsinsgesamt;
	}

	public void setPingsinsgesamt(long pingsinsgesamt) {
		Pingsinsgesamt = pingsinsgesamt;
	}

	public long getGefundenePakete() {
		return gefundenePakete;
	}

	public void setGefundenePakete(long gefundenePakete) {
		this.gefundenePakete = gefundenePakete;
	}

	public long getVerlorenePakete() {
		return verlorenePakete;
	}

	public void setVerlorenePakete(long verlorenePakete) {
		this.verlorenePakete = verlorenePakete;
	}

	public ArrayList<String> getTimesstamps() {
		return timesstamps;
	}

	public void setTimesstamps(ArrayList<String> timesstamps) {
		this.timesstamps = timesstamps;
	}

    public JButton getButton() {
        return Button;
   }

    public void setButton(JButton button) {
        Button = button;
    }

    public EinstellungenPings getEinstellungenPings() {
        return einstellungenPings;
    }

    public void setEinstellungenPings(EinstellungenPings einstellungenPings) {
        this.einstellungenPings = einstellungenPings;
    }

    public boolean isRunstatus() {
        return runstatus;
    }

    public void setRunstatus(boolean runstatus) {
        this.runstatus = runstatus;
    }

    public Object getItemOnPosition(int pos) {
        if(pos == 0) {
            return this.getIPAddress();
        }
        if(pos == 1) {
            return Button.getText();
        }
        if(pos == 2) {
            return this.getGefundenePakete();
        }
        if(pos == 3) {
            return this.getVerlorenePakete();
        }
        return null;
    }

    public Object setItemOnPosition(int pos, Object newValue) {
        if(pos == 0) {
            this.setIPAddress(String.valueOf(newValue));
        }
        if(pos == 1) {

        }
        if(pos == 2) {
            this.setGefundenePakete(Long.valueOf(String.valueOf(newValue)));
        }
        if(pos == 3) {
            this.setVerlorenePakete(Long.valueOf(String.valueOf(newValue)));
        }
        return null;
    }


}
