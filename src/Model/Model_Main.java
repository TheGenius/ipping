package Model;

import java.util.ArrayList;
import java.util.Observable;

public class Model_Main extends Observable{
	
	String[] ColumnNames = {"IP-Adresse","","Gesendete Pakete","Verlorene Pakete"};
    ArrayList<PingRow> listederPingRows = new ArrayList<>();
    EinstellungenPings defaultEinstellungen = new EinstellungenPings();
	public String[] getColumnNames() {
		return ColumnNames;
	}

	public void setColumnNames(String[] columnNames) {
		ColumnNames = columnNames;
	}

	public ArrayList<PingRow> getListederPingRows() {
		return listederPingRows;
	}
	
	public PingRow getPingsderListederPings(int pos) {
		return listederPingRows.get(pos);
	}
	
	public PingRow getPingsderListederPings(PingRow PingRow) {
		return listederPingRows.get(listederPingRows.indexOf(PingRow));
	}
	
	public void setListederPingRows(ArrayList<PingRow> listederPingRows) {
		this.listederPingRows = listederPingRows;
	}
	
	public void setListederPings(int pos,PingRow pingzeile) {
        listederPingRows.set(pos, pingzeile);
        setChanged();
        notifyObservers(pingzeile);
	}

    public void overrideallSettings(EinstellungenPings overrideeinstellungenPings){
        for(int i =0; i< this.getListederPingRows().size();i++){
            this.getListederPingRows().get(i).setEinstellungenPings(overrideeinstellungenPings);
        }
    }

    public EinstellungenPings getDefaultEinstellungen() {
        return defaultEinstellungen;
    }

    public void setDefaultEinstellungen(EinstellungenPings defaultEinstellungen) {
        this.defaultEinstellungen = defaultEinstellungen;
    }
}
