package View;

import Model.EinstellungenPings;
import Model.Model_Main;

import javax.swing.*;
import java.awt.event.*;

public class JDialog_PingEinstellungen extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel jL_PingAnzahl;
    private JTextField jTF_InputAnzahlPings;
    private JLabel jL_UnendlichPingen;
    private JCheckBox cB_unendlichPingen;
    private JLabel jL_Port;
    private JTextField jtF_InputPort;
    private Model_Main model_main;
    public JDialog_PingEinstellungen(Model_Main m_MM) {
        setContentPane(contentPane);
        setModal(true);
        this.setTitle("Einstellungen zu den Pings");
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        model_main=m_MM;

        //Initialisierung der Felder
        jTF_InputAnzahlPings.setText(String.valueOf(model_main.getDefaultEinstellungen().getAnzahlPings()));
        jtF_InputPort.setText(String.valueOf(model_main.getDefaultEinstellungen().getPort()));
        cB_unendlichPingen.setSelected(model_main.getDefaultEinstellungen().isUnendlichPingen());

        this.pack();
        this.setVisible(true);
    }

    private void onOK() {
        //Wenn Enter oder der OK Button geklickt wird werden die DefaultEinstellungen mit der jede Zeile erstellt wird ge�ndert.
        EinstellungenPings eP = new EinstellungenPings(cB_unendlichPingen.isSelected(),Integer.valueOf(jTF_InputAnzahlPings.getText().trim()),Integer.valueOf(jtF_InputPort.getText().trim()));
        model_main.setDefaultEinstellungen(eP);
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }
}



