package View;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Model.Model_Main;
import Model.PingRow;

@SuppressWarnings("serial")
public class PingTableModel extends AbstractTableModel {
	
	private Model_Main MM;
	private String[] columnNames;
    private ArrayList<PingRow> data = new ArrayList<>();
	public PingTableModel(Model_Main mm) {
		this.MM = mm;
        columnNames = this.MM.getColumnNames();
	}

    public void addRow(PingRow rowData) {
        data.add(rowData);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.size(); // length
        // can be solved via .size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
//          change made here
        PingRow rowElement = data.get(row);
        Object value = rowElement.getItemOnPosition(col);
        return value;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return col==0||col==1;
    }

    public void setValueAt(Object value, int row, int col) {
        // change made here
        PingRow rowElement = data.get(row);
        rowElement.setItemOnPosition(col, value);
        fireTableCellUpdated(row, col);
    }

    public ArrayList<PingRow> getData() {
        return data;
    }

    public PingRow getData(int pos) {
        return data.get(pos);
    }

    public void setData(ArrayList<PingRow> data) {
        this.data = data;
    }


}



