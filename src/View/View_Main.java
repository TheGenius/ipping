package View;

import java.util.Observable;
import java.util.Observer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import ActionListener.NewRow;
import ActionListener.TableMouseListener;
import Model.Model_Main;
import Model.PingRow;

import java.awt.BorderLayout;

public class View_Main extends JPanel implements Observer {
	Model_Main model_main;
	public JTable table_Ping;


	public View_Main(Model_Main MM) {
		this.model_main = MM;
		this.model_main.addObserver(this);
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);

		DefaultTableModel tableModel = new DefaultTableModel() {

			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				// Wenn der SpaltenIndex kleiner als drei ist soll die Spalte
				// nicht editierbar sein
				if (columnIndex > 1) {
					return false;
				} else {
					return true;
				}
			}


		};

		tableModel.setColumnIdentifiers(MM.getColumnNames());
		JButton btnNeueZeileHinzufgen = new JButton("Neue Zeile hinzuf\u00FCgen");
		add(btnNeueZeileHinzufgen, BorderLayout.NORTH);

		PingTableModel pingTableModelAbstract = new PingTableModel(this.model_main);

		table_Ping = new JTable(pingTableModelAbstract);
        table_Ping.getTableHeader().setReorderingAllowed(false);
        TableMouseListener tableMouseListener = new TableMouseListener(table_Ping);
        table_Ping.addMouseListener(tableMouseListener);
		scrollPane.setViewportView(table_Ping);

		NewRow newRow = new NewRow(table_Ping, model_main);
		btnNeueZeileHinzufgen.addActionListener(newRow);

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg0 instanceof Model_Main){
            Model_Main mMainU = (Model_Main) arg0;


            if(arg1 instanceof PingRow){
                //Gesendete Pakete
              //  long defaultValuePings = (Long)table_Ping.getValueAt(((PingRow) arg1).getRownumber(),2);
             //   long defaultValueTimeouts = (Long)table_Ping.getValueAt(((PingRow) arg1).getRownumber(),3);
                table_Ping.setValueAt((((PingRow) arg1).getPingsinsgesamt()),((PingRow) arg1).getRownumber(),2);
                //Timeouts
                table_Ping.setValueAt((((PingRow) arg1).getVerlorenePakete()),((PingRow) arg1).getRownumber(),3);
            }
        }
	}
}
