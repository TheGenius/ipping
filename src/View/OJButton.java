package View;

import javax.swing.*;

/**
 * Created by pludewig on 25.04.2016.
 */
public class OJButton extends JButton {

    int id;
    public OJButton(int id){
        super();
        this.id=id;
    }

    public int getId() {
        return id;
    }
}
