package Controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import Model.Model_Main;
import View.View_Main;

public class Controller {
	JFrame target;
    public final static String pingChanged ="";
    public final static String ChCAnzPings ="";

	public Controller(JFrame target, double Versionsnummer) {

		
		
		Model_Main modelMain = new Model_Main();

		View_Main viewMain = new View_Main(modelMain);
		
		this.target = target;
		this.target.setContentPane(viewMain);
		JMenuBar jMenuBar = new JMenuBar();
		JMenu mainMenu = new JMenu("Hauptmen\u00FC");
        JMenuItem einstellungen = new JMenuItem("Einstellungen");
        einstellungen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                View.JDialog_PingEinstellungen jDialog_pingEinstellungen = new View.JDialog_PingEinstellungen(modelMain);
            }
        });
		JMenuItem close = new JMenuItem("Close");
		close.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Controller.this.target.dispose();
            }
        });
		jMenuBar.add(mainMenu);
        mainMenu.add(einstellungen);
		mainMenu.add(close);
		this.target.setJMenuBar(jMenuBar);
		this.target.pack();
		this.target.setLocationRelativeTo(null);
		this.target.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.target.setTitle("IP-PingStatistik v" + Versionsnummer);
		this.target.setVisible(true);
		
	}

}
