package Ping;

import Model.EinstellungenPings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by pludewig on 22.04.2016.
 */
public class Ping_Help {

    private String ipAddress;
    private boolean status;
    private EinstellungenPings einstellungenPings;

    Ping_Help(String InetAddress, EinstellungenPings eP) {
        this.setIpAddress(InetAddress);
        //Es kann nur offene Ports gepingt werden. In einem Netzwerk indem sämtliche Ports gesperrt sind geht das nicht.
        this.setStatus(this.isReachable(InetAddress, eP.getPort(), 5000) && !InetAddress.isEmpty());
        //this.setStatus(this.isReachablePings());
        this.einstellungenPings = eP;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public String getPingCommand() {
        if (einstellungenPings.isUnendlichPingen() == false) {
            String command = "ping " + getIpAddress() + " -n " + einstellungenPings.getAnzahlPings();
            return command;
        } else {
            String command = "ping " + getIpAddress() + " -t ";
            return command;
        }

    }

    public boolean isReachable(String addr, int openPort, int timeOutMillis) {
        // Any Open port on other machine
        // openPort =  22 - ssh, 80 or 443 - webserver, 25 - mailserver etc.
        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(addr, openPort), timeOutMillis);
            }
            return true;
        } catch (IOException ex) {
            System.out.println("Not Reachable");
            return false;
        }
    }

    public boolean isReachablePings() {
        long pings = 0;
        long timeouts = 0;
        int other = 0;
        try {
            //create and execute a system command
            String command = "ping " + getIpAddress();
            Process sysProcess = Runtime.getRuntime().exec(command);

            //crate a input stream reader
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(sysProcess.getInputStream()));

            //standard Pings is 4


            //read the input stream line by line
            String line;
            //System.out.println("Reading the input stream REACHABLE");
            while ((line = streamReader.readLine()) != null) {
                //print each line is read
                // System.out.println(line);
                if (line.startsWith("Reply from") || line.startsWith("64 Bytes from") || line.startsWith("Antwort von") || line.startsWith("64 Bytes von")) {
                    pings++;
                    return true;
                } else {
                    if (line.startsWith("Request timed out") || line.contains("Zielhost nicht erreichbar.")|| line.contains("Zeit\\u00fcberschreitung der Anforderung.")) {
                        timeouts++;
                        return false;
                    } else {
                        other++;
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        if(pings==4){
            return true;
        }else{
            if(timeouts>0) {
                return false;
            }else{
                return false;
            }
        }
    }
}


