package Ping;

import Model.Model_Main;
import Model.PingRow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ping_Thread extends Thread {
    public PingRow pingRowObject;
    public boolean status;
    private Ping_Help PingH;
    Model_Main modelMain;
    public Ping_Thread(PingRow pingRowInformation, Model_Main modelMain) {
        pingRowObject = pingRowInformation;
        PingH = new Ping_Help(pingRowObject.getIPAddress(),pingRowObject.getEinstellungenPings());
        status = PingH.isStatus();
        this.modelMain=modelMain;
    }

    public void run() {

        while (!interrupted()&&pingRowObject.isRunstatus()) {
            try {
                //create and execute a system command
                Process sysProcess = Runtime.getRuntime().exec(PingH.getPingCommand());

                //crate a input stream reader
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(sysProcess.getInputStream()));

                //standard Pings is 4
                long pings = pingRowObject.getPingsinsgesamt();
                long timeouts = pingRowObject.getVerlorenePakete();
                int other = 1;

                //read the input stream line by line
                String line;
                //System.out.println("Reading the input stream");
                while ((line = streamReader.readLine()) != null&&!interrupted()) {
                    //print each line is read
                    System.out.println(line);
                    if ((line.startsWith("Reply from") || line.startsWith("64 Bytes from") || line.startsWith("Antwort von") || line.startsWith("64 Bytes von")|line.contains("Zielhost nicht erreichbar")|| line.contains("Zeit\\u00fcberschreitung der Anforderung."))) {
                        pingRowObject.setPingsinsgesamt(pings);
                        modelMain.setListederPings(pingRowObject.getRownumber(), pingRowObject);
                        pings++;
                    } else {
                        if (line.startsWith("Request timed out")||line.contains("Zielhost nicht erreichbar")|| line.contains("Zeit\\u00fcberschreitung der Anforderung.")) {
                            System.out.println(timeouts);
                            pingRowObject.setVerlorenePakete(timeouts);
                            modelMain.setListederPings(pingRowObject.getRownumber(), pingRowObject);
                            timeouts++;
                        } else {
                            other++;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isStatus() {
        return status;
    }
}


