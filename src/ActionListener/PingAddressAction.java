package ActionListener;

import Model.Model_Main;
import Model.PingRow;
import Ping.Ping_Thread;
import View.PingTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by pludewig on 27.04.2016.
 */
public class PingAddressAction implements Action {
    Model_Main modelMain;
    Ping_Thread ping_thread;
    public PingAddressAction(Model_Main modelMain) {
        this.modelMain = modelMain;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JTable table = (JTable) e.getSource();

        PingTableModel modelA = (PingTableModel) table.getModel();
        //Get the row number from the event actioncommand and the object from the tablemodel

        PingRow pingRowObject= modelA.getData(Integer.valueOf(e.getActionCommand()));

        //Button wird gedr�ckt
          if(!pingRowObject.isRunstatus()) {
           pingRowObject.setRunstatus(!pingRowObject.isRunstatus());
             ping_thread = new Ping_Thread(pingRowObject, this.modelMain);
              //TODO STATUS wird nicht korrekt erkannt
              System.out.println("Der Status ist: "+ping_thread.isStatus());
                if (ping_thread.isStatus()) {
                    ping_thread.start();
                }else{
                    //Hier wird der Boolean wieder umgedreht damit im Falle einer nicht erreichbaren Adresse erneut gepingt werden kann.
                    pingRowObject.setRunstatus(!pingRowObject.isRunstatus());
                }
          }else{
                    ping_thread.interrupt();
                    pingRowObject.setRunstatus(!pingRowObject.isRunstatus());
          }
    }

    @Override
    public Object getValue(String key) {
        return null;
    }

    @Override
    public void putValue(String key, Object value) {

    }

    @Override
    public void setEnabled(boolean b) {

    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {

    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {

    }

    ;
}