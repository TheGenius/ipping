package ActionListener;

import Model.Model_Main;
import Model.PingRow;
import View.ButtonColumn;
import View.OJButton;
import View.PingTableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class NewRow implements ActionListener{
	JTable table;

    Model_Main mMain;
	public NewRow(JTable table_Ping, Model_Main MM) {
	    table = table_Ping;
        mMain = MM;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
        //Get the tablemodel
        PingTableModel modelA = (PingTableModel) table.getModel();
        //JButton definition
        OJButton tmp = new OJButton(table.getRowCount());
        tmp.setText("Starte Ping");
        //Instance a object for the new row
        PingRow pingRowObject = new PingRow("",tmp,0,0,table.getRowCount(),mMain.getDefaultEinstellungen());
        //Add the object to the tablemodel
        modelA.addRow(pingRowObject);
        mMain.getListederPingRows().add(pingRowObject);
        //Instance the action
        PingAddressAction pingAddressAction = new PingAddressAction(mMain);
        //Rob Carmick Class for Button in Column
        ButtonColumn buttonColumn = new ButtonColumn(table,pingAddressAction,1);
    }

}
